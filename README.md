# Shared bibliographies

This repository contains a collection of LaTeX bibliography
entries. The entries themselfes are mostly descriptive information
that is in the public domain. If any parts of this repository
constitute a copyrighted work, the are licensed under the conditions
of the [CC BY
4.0](https://creativecommons.org/licenses/by/4.0/deed.de) license.

## Updating the bibliography and the README index in one step

Before adding the new entry, edit the bibtex file to set the citation key.

The script `./Scripts/add-entry.py` performs all steps required to add
a new bibliography entry in one step:

    > python3 ./Scripts/add-entry.py ~/Downloads/S0048733323001300.bib
	...

Only commit the new entry to the index, not all changes update to the
README. Writing the normalized bibtex file changes some of the
encoding.

## Inserting bibliography entries with sorting and normalization

The script `./Scripts/sort-bibliography.py` uses the Python bibtex parser to load multiple bibtex files specified on the command line, normalize encoding and syntax and print them sorted by the entry ID:

    > python3 ./Scripts/sort-bibliography.py Bibliography.bib ~/Downloads/new-bibtex-entry.bib
    ...

Inspect the output and use it to commit new entries in the main bibliography. When called with the `--format=md` argument, the script prints a Markdown table of the entries sorted by entry id:

    > python3 ./Scripts/sort-bibliography.py --format Bibliography.bib
    ...

## Entries

| # | ID | Author/Editor | Title | Type |
| --:|:-- |:-- |:-- |:-- |
| 0 | mb:Pentheroudakis-et-al-2017 | Pentheroudakis, C and Baron, JA and Thumm, N | Licensing Terms of Standard Essential Patents: A Comprehensive Analysis of Cases | techreport |
| 1 | mb:aghion-et-al-2005 | Aghion, Philippe and Bloom, Nick and Blundell, Richard and Griffith, Rachel and Howitt, Peter | Competition and Innovation: an Inverted-U Relationship | article |
| 2 | mb:almeida-et-al-2011 | Almeida, O. and Oliveira, José and Cruz, José | Open Standards and Open Source: Enabling interoperability | article |
| 3 | mb:apache.org-way | The Apache Software Foundation | Briefing: The Apache Way | online |
| 4 | mb:ariely-et-al-2018 | Ariely, Dan and Gneezy, Uri and Haruvy, Ernan | Social norms and the price of zero | article |
| 5 | mb:bacon-2012 | Bacon, Jono | The Art of Community: Building the New Age of Participation (Theory in Practice) | book |
| 6 | mb:bain-mccoy-2022 | Bain, Malcolm and Smith, P McCoy | Patents and the Defensive Response | incollection |
| 7 | mb:baldwin-clark-2006 | Baldwin, Carliss Y. and Clark, Kim B. | The Architecture of Participation: Does Code Architecture Mitigate Free Riding in the Open Source Development Model? | article |
| 8 | mb:baron-et-al-2011 | Justus Baron and Knut Blind and Tim Pohlmann | Essential patents and standard dynamics | inproceedings |
| 9 | mb:baron-et-al-2019 | Baron, J and Larouche, P and Contreras, J and Thumm, N and Husovec, M | Making the rules – The governance of standard development organizations and their policies on intellectual property rights | book |
| 10 | mb:baron-spulber-2018 | Baron, Justus and Spulber, Daniel F. | Technology Standards and Standard Setting Organizations: Introduction to the Searle Center Database | article |
| 11 | mb:bekkers-updegrove-2012 | Bekkers, Rudi and Updegrove, Andrew | A Study of IPR Policies and Practices of a Representative Group of Standards Setting Organizations Worldwide | article |
| 12 | mb:bell-1972 | Bell, Daniel | On meritocracy and equality | article |
| 13 | mb:benkler-2002 | Benkler, Yochai | Coase's Penguin, or, Linux and "The Nature of the Firm" | article |
| 14 | mb:berne-1967 | World Intellectual Property Organization | Berne Convention for the Protection of Literary and Artistic Works | online |
| 15 | mb:bessen-2006 | James Bessen | Open Source Software: Free Provision of Complex Public Goods | incollection |
| 16 | mb:bevir-2012 | Bevir, Mark | Governance - a very short introduction | book |
| 17 | mb:blind-2002 | Blind, Knut | Driving forces for standardization at standardization development organizations | article |
| 18 | mb:blind-2011 | Knut Blind | An economic analysis of standards competition: The example of the ISO ODF and OOXML standards | article |
| 19 | mb:blind-boehm-2019 | Blind, Knut and Boehm, Mirko | The Relationship Between Open Source Software and Standard Setting | report |
| 20 | mb:blind-et-al-2002 | Blind, Knut and Bierhals, R and Thumm, N and Hossein, K and Sillwood, John and Iversen, Eric and Reekum, R van and Rixius, Bruno | Study on the interaction between standardisation and intellectual property rights | article |
| 21 | mb:blind-et-al-2021 | Blind, K and Pätsch, S and Muto, S and Böhm, M and Schubert, T and Grzegorzewska, P and Katz, A, and European Commission and Directorate-General for Communications Networks, Content and Technology | The impact of open source software and hardware on technological independence, competitiveness and innovation in the EU economy – Final study report | book |
| 22 | mb:blind-et-al-2022 | Blind, Knut and Böhm, Mirko and Thumm, Nikolaus | Open Source Software in Standard Setting: The Role of Intellectual Property Right Regimes | incollection |
| 23 | mb:blind-mangelsdorf-2016 | Blind, Knut and Mangelsdorf, Axel | Motives to standardize: Empirical evidence from Germany | article |
| 24 | mb:boehm-2019 | Mirko Boehm | The emergence of governance norms in volunteer driven open source communities | article |
| 25 | mb:boehm-2022 | Boehm, Mirko | Economics of Open Source | incollection |
| 26 | mb:boehm-eisape-2019 | Boehm, Mirko and Eisape, Davis | Normungs- und Standardisierungsorganisationen und Open Source Communities - Partner oder Wettbewerber? | incollection |
| 27 | mb:boehm-eisape-2021 | Boehm, Mirko and Eisape, Davis | Standard setting organizations and open source communities: Partners or competitors? | article |
| 28 | mb:bolt-et-al-2018 | Bolt, Jutta and Inklaar, Robert and de Jong, Herman and van Zanden, Jan Luiten | Rebasing ‘Maddison’: new income comparisons and the shape of long-run economic development | article |
| 29 | mb:bradford-2020 | Bradford, Anu | The Brussels effect: How the European Union rules the world | book |
| 30 | mb:brock-2022 | Brock, Amanda | Open Source Law, Policy and Practice | book |
| 31 | mb:brock-models-2022 | Brock, Amanda | Business and Revenue Models and Commercial Agreements | incollection |
| 32 | mb:capra-et-al-2008 | Capra, Eugenio and Francalanci, Chiara and Merlo, Francesco | An Empirical Study on the Relationship Between Software Design Quality, Development Effort and Governance in Open Source Projects | article |
| 33 | mb:carter-groopman-2021 | Carter, Hilary and Groopman, Jessica | Diversity, Equity, and Inclusion in Open Source: Exploring the Challenges and Opportunities to Create Equity and Agency Across Open Source Ecosystems | report |
| 34 | mb:casari-et-al-2023 | Casari, Amanda and Ferraioli, Julia and Lovato, Juniper | Beyond the Repository | article |
| 35 | mb:cheng-et-al-2011 | Hsing Kenneth Cheng and Yipeng Liu and Qian (Candy) Tang | The Impact of Network Externalities on the Competition Between Open Source and Proprietary Software | article |
| 36 | mb:chesbrough-2006 | Chesbrough, Henry and Vanhaverbeke, Wim and West, Joel | Open innovation: Researching a new paradigm | book |
| 37 | mb:chesbrough-2006-business-models | Chesbrough, Henry | Open business models: How to thrive in the new innovation landscape | book |
| 38 | mb:chestek-2022 | Chestek, Pamela | Trademarks | incollection |
| 39 | mb:churchill-2016 | The International Churchill Society | The worst form of government | online |
| 40 | mb:clark-2017 | Clark, Jamie | OASIS at ITU/NGMN: Convergence, Collaboration and Smart Shopping in Open Standards and Open Source | online |
| 41 | mb:coase-1937 | Coase, Ronald H. | The Nature of the Firm | article |
| 42 | mb:comino-et-al-2019 | Comino, Stefano and Manenti, Fabio M and Thumm, Nikolaus | The role of patents in information and communication technologies: A survey of the literature | article |
| 43 | mb:cooper-2016 | Robert G. Cooper | Agile-Stage-Gate Hybrids | article |
| 44 | mb:corbet-2017 | Corbet, J and Kroah-Hartman, G | 2017 Linux Kernel Development Report | article |
| 45 | mb:csikszentmihalyi-1996 | Csikszentmihalyi, Mihaly | Creativity: The work and lives of 91 eminent people | book |
| 46 | mb:curto--2023 | Curto-Millet, Daniel and Jiménez, Alberto Corsín | The sustainability of open source commons | article |
| 47 | mb:daffara-2012 | Daffara, Carlo | Estimating the Economic Contribution of Open Source Software to the European Economy | inbook |
| 48 | mb:de-vries-et-al-2008 | de Vries, Huibert and de Vries, Henk and Oshri, Ilan | Standards Battles in Open Source Software: The Case of Firefox | book |
| 49 | mb:demsetz-1974 | Demsetz, Harold | Toward a theory of property rights | incollection |
| 50 | mb:dibona-ockman-1999 | DiBona, Chris and Ockman, Sam | Open sources: Voices from the open source revolution | book |
| 51 | mb:din-2017-nl | DIN e.V. | DIN-Normenausschuss Luft- und Raumfahrt, Jahresbericht 2017 | online |
| 52 | mb:dixit-olson-2000 | Dixit, Avinash and Olson, Mancur | Does voluntary participation undermine the Coase Theorem? | article |
| 53 | mb:ec-2016 | European Commission | Implementing FRAND standards in Open Source: Business as usual or mission impossible? Report on the joint European Commission and European Patent Office conference "Implementing FRAND standards in Open Source" on 22 November 2012 | report |
| 54 | mb:ec-2017-interop | European Commission | European Interoperability Framework | book |
| 55 | mb:ec-2017-sep | European Commission | Communication from the Commission to the European Parliament, the Council and the European Economic and Social Committee: Setting out the EU approach to Standard Essential Patents | book |
| 56 | mb:eghbal-2018 | Eghbal, Nadia | Roads and bridges: the unseen labor behind our digital infrastructure | misc |
| 57 | mb:etzioni-1960 | Amitai Etzioni | Two Approaches to Organizational Analysis: A Critique and a Suggestion | article |
| 58 | mb:fehr-et-al-2002 | Fehr, Ernst and Fischbacher, Urs and Gächter, Simon | Strong reciprocity, human cooperation, and the enforcement of social norms | article |
| 59 | mb:fogel-2005 | Fogel, Karl | Producing Open Source Software: How to Run a Successful Free Software Project | book |
| 60 | mb:folmer-et-al-2009 | Folmer, Erwin and van Bekkum, Michael and Verhoosel, Jack | Strategies for using international domain standards within a national context: The case of the Dutch temporary staffing industry | inproceedings |
| 61 | mb:fsa-2017 | Fair Standards Alliance | The importance of maintaining the open source software value proposition | online |
| 62 | mb:fsf-1996 | Free Software Foundation | What is Free Software? | online |
| 63 | mb:gamalielsson-et-al-2015 | Jonas Gamalielsson and Björn Lundell and Jonas Feist and Tomas Gustavsson and Fredric Landqvist | On organisational influences in software standards and their open source implementations | article |
| 64 | mb:gans-et-al-2000 | Gans, Joshua S and Hsu, David H and Stern, Scott | When does start-up innovation spur the gale of creative destruction? | techreport |
| 65 | mb:gasser-palfrey-2007 | Gasser, Urs and Palfrey, John | When and How ICT Interoperability Drives Innovation | article |
| 66 | mb:gay-2015 | Joshua Gay | The Principles of Community-Oriented GPL Enforcement | online |
| 67 | mb:ghosh-2011 | Ghosh, Rishab A | An economic basis for open standards | article |
| 68 | mb:gordon-2003 | Gordon, Wendy J. | Intellectual Property | article |
| 69 | mb:hansen-1985 | Hansen, John M. | The Political Economy of Group Membership | article |
| 70 | mb:hardin-1968 | Garett Hardin | The Tragedy of the Commons | article |
| 71 | mb:hardin-1982 | Hardin, Russell | Collective Action | book |
| 72 | mb:hartman-et-al-2017 | Kroah-Hartman, Greg and Mason, Chris and van Riel, Rik and Khan, Shuah and Likely, Grant | Linux Kernel Community Enforcement Statement | online |
| 73 | mb:harvard-business-2005 | Harvard Business Essentials | Strategy: Create and implement the best strategy for your business | article |
| 74 | mb:heller-1998 | Heller, Michael A | The tragedy of the anticommons: property in the transition from Marx to markets | article |
| 75 | mb:hemel-coughlan-2017 | Hemel, Armijn and Coughlan, Shane | Practical GPL Compliance | book |
| 76 | mb:henkel-et-al-2014 | Henkel, Joachim and Schöberl, Simone and Alexy, Oliver | The emergence of openness: How and why firms adopt selective revealing in open innovation | article |
| 77 | mb:herzberg-1993 | Herzberg, F. and Mausner, B. and Snyderman, B. B. | The Motivation to Work | book |
| 78 | mb:hirschman-1970 | Hirschman, A. O. | Exit, Voice, and Loyalty: Responses to Decline in Firms, Organizations, and States | book |
| 79 | mb:hirschman-1997 | Hirschman, Albert O. | The Passions and the Interests | book |
| 80 | mb:hoffman-mehra-2009 | Hoffman, David A and Mehra, Salil K | Wikitruth through wikiorder | article |
| 81 | mb:hoffmann-et-al-2024 | Hoffmann, Manuel and Nagle, Frank and Zhou, Yanuo | The Value of Open Source Software | article |
| 82 | mb:houben-et-al-1999 | Houben, G. and Lenie, K. and Vanhoof, K. | A knowledge-based SWOT-analysis system as an instrument for strategic planning in small and medium sized enterprises | article |
| 83 | mb:hunter-walli-2013 | Paula Hunter and Stephen Walli | The Rise and Evolution of the Open Source Software Foundation | article |
| 84 | mb:jenkins-2001 | Rhys Jenkins | Corporate Codes of Conduct - Self-Regulation in a Global Economy | article |
| 85 | mb:johnny-et-al-2010 | Omar Johnny and Marc Miller and Mark Webbink | Copyright in Open Source Software - Understanding the Boundaries | article |
| 86 | mb:josh-tirole-2015 | Lerner, Josh and Tirole, Jean | Standard-Essential Patents | article |
| 87 | mb:kappos-2016 | Kappos, David J | Open source software and standards development organizations: symbiotic functions in the innovation equation | article |
| 88 | mb:kappos-2018 | Kappos, David J and Harrington, Miling Y | The Truth about OSS-FRAND: By All Indications, Compatible Models in Standards Settings | article |
| 89 | mb:katz-2022 | Katz, Andrew | Open Hardware | incollection |
| 90 | mb:katz-shapiro-1986 | Katz, Michael L. and Shapiro, Carl | Technology Adoption in the Presence of Network Externalities | article |
| 91 | mb:kde-20-years | Pintscher, Lydia | 20 Years of KDE: Past, Present and Future | book |
| 92 | mb:kemp-2009 | Richard Kemp | Towards Free/Libre Open Source Software (“FLOSS”) Governance in the Organisation | article |
| 93 | mb:knuth-1974 | Knuth, Donald E | Computer programming as an art | article |
| 94 | mb:laffan-2012 | Laffan, Liz | A New Way of Measuring Openness: The Open Governance Index | article |
| 95 | mb:lakhani-wolf-2003 | Lakhani, Karim and Wolf, Robert G. | Why Hackers Do What They Do: Understanding Motivation and Effort in Free/Open Source Software Projects | article |
| 96 | mb:lawson-hendrick-2023 | Lawson, Adrienn and Hendrick, Stephen | Global Spotlight 2023: Survey-based insights into the global landscape of open source trends, sustainability challenges, and growth opportunities | report |
| 97 | mb:lerner-tirole-2000 | Lerner, Josh and Triole, Jean | The Simple Economics of Open Source | article |
| 98 | mb:lerner-tirole-2005 | Lerner, Josh and Tirole, Jean | The economics of technology sharing: Open source and beyond | article |
| 99 | mb:lessig-2009 | Lessig, Lawrence | Code: And other laws of cyberspace | book |
| 100 | mb:leveque-meniere-2005 | Leveque, Francois and Ménière, Yann | The Economics of Patents and Copyright | article |
| 101 | mb:li-2017 | Li, Jingze | Intellectual property licensing tensions in incorporating open source into formal standard setting context — The case of Apache V.2 in ETSI as a start | inproceedings |
| 102 | mb:linaaker-et-al-2016 | Linåker, Johan and Rempel, Patrick and Regnell, Björn and Mäder, Patrick | How Firms Adapt and Interact in Open Source Ecosystems: Analyzing Stakeholder Influence and Collaboration Patterns | inproceedings |
| 103 | mb:linux-foundation-2016 | Corbet, Jonathan and Kroah-Hartman, Greg | Linux Kernel Development, 25th Anniversary Edition | techreport |
| 104 | mb:linux-foundation-2018 | Parker-Johnson, Paul and Doiron, Tim | Succeeding on an Open Field: The Impact of Open Source Technologies on the Communication Service Provider Ecosystem | booklet |
| 105 | mb:linux-foundation-2023-09-opentofu | The Linux Foundation | Linux Foundation Launches OpenTofu: A New Open Source Alternative to Terraform | online |
| 106 | mb:linux-foundation-licquia-mcpherson-2016 | Licquia, Jeff and McPherson, Amanda | A \$5 Billion Value: Estimating the Total Development Cost of Linux Foundation's Collaborative Projects | techreport |
| 107 | mb:lovejoy-2022 | Lovejoy, Jilayne | Contributor Agreements | incollection |
| 108 | mb:lundell-2017 | Lundell, Björn and Gamalielsson, Jonas | On the potential for improved standardisation through use of open source work practices in different standardisation organisations: How can open source-projects contribute to development of IT-standards? | inproceedings |
| 109 | mb:lundell-et-al-2015 | Lundell, Björn and Gamalielsson, Jonas and Katz, Andrew | On implementation of Open Standards in software : To what extent can ISO standards be implemented in open source software? | article |
| 110 | mb:lundell-et-al-2019 | Björn Lundell and Jonas Gamalielsson and Andrew Katz | Implementing IT Standards in Software: Challenges and Recommendations for Organisations Planning Software Development Covering IT Standards | article |
| 111 | mb:luxbacher-2017 | Luxbacher, Günther | DIN von 1917 bis 2017 | book |
| 112 | mb:mahony-2007 | O'Mahony, Siobhán | The governance of open source initiatives: what does it mean to be community managed? | article |
| 113 | mb:mahony-ferraro-2007 | O'Mahony, Siobhán and Ferraro, Fabrizio | The Emergence of Governance in an Open Source Community | article |
| 114 | mb:mankur-1965 | Mancur, Olson | The Logic of Collective Action: Public Goods and the Theory of Groups | book |
| 115 | mb:maracke-2019 | Maracke, Catharina | Free and Open Source Software and FRAND-based patent licenses | article |
| 116 | mb:markus-2007 | Markus, M. Lynne | The governance of free/open source software projects: monolithic, multidimensional, or configurational? | article |
| 117 | mb:meeker-2017 | Heather Meeker | Patrick McHardy and copyright profiteering | online |
| 118 | mb:meuser-1989 | Meuser, Michael and Nagel, Ulrike | ExpertInneninterviews—vielfach erprobt, wenig bedacht | incollection |
| 119 | mb:miralles-et-al-2006 | Miralles, Francesc and Sieber, Sandra and Valor, Josep | An Exploratory Framework for Assessing Open Source Software Adoption | article |
| 120 | mb:mitchell-mason-2011 | Mitchell, Iain and Mason, Stephen | Compatibility Of The Licensing Of Embedded Patents With Open Source Licensing Terms | article |
| 121 | mb:nagle-2018 | Nagle, Frank | Open source software and firm productivity | article |
| 122 | mb:nagle-2019 | Nagle, Frank | Government Technology Policy, Social Value, and National Competitiveness | article |
| 123 | mb:nagle-et-al-2020 | Frank Nagle and Jessica Wilkerson and James Dana and Jennifer L. Hoffman | Vulnerabilities in the Core: Preliminary Report and Census II of Open Source Software | report |
| 124 | mb:nagle-et-al-2022 | Nagle, Frank and Dana, James and Hoffman, Jennifer and Randazzo, Steven and Zhou, Yanuo | Census II of Free and Open Source Software - Application Libraries | report |
| 125 | mb:oecd-2011 | OECD | Demand-side Innovation Policies | book |
| 126 | mb:oecd-2018 | OECD and Eurostat | Oslo Manual 2018 | book |
| 127 | mb:ofe-2017-standards-oss | OpenForum Europe | Standards and Open Source - Bringing them together | online |
| 128 | mb:opensourceway-2018 | opensource.com | The open source way | online |
| 129 | mb:osborne-et-al-2023 | Osborne, Cailean and Boehm, Mirko and Jimenez Santamaria, Ana | The European Public Sector Open Source Opportunity: Challenges and Recommendations for Europe’s Open Source Future | report |
| 130 | mb:osi-osd-2006 | Open Source Initiative | The Open Source Definition | online |
| 131 | mb:ostrom-1990 | Ostrom, Elinor | Governing the commons: the evolution of institutions for collective action | book |
| 132 | mb:ostrom-2000 | Ostrom, Elinor | Collective Action and the Evolution of Social Norms | article |
| 133 | mb:perens-2017 | Bruce Perens | On Usage of The Phrase "Open Source" | online |
| 134 | mb:peter-2006 | Ullrich, Peter | Das explorative ExpertInneninterview: Modifikationen und konkrete Umsetzung der Auswertung von ExpertInneninterviews nach Meuser-Nagel | article |
| 135 | mb:phipps-2011 | Simon Phipps | Open source procurement: Copyrights | online |
| 136 | mb:phipps-2019 | Phipps, Simon | Why Legal Issues are the Wrong Lens to understand the Open Source and FRAND issue | online |
| 137 | mb:pohlmann-2013 | Pohlmann, Tim C. | Attributes and Dynamic Development Phases of Informal ICT Standards Consortia | article |
| 138 | mb:popper-1957 | Popper, Karl Raimund | The open society and its enemies | book |
| 139 | mb:raymond-1996 | Raymond, Eric S and Steele, Guy L | The new hacker's dictionary | book |
| 140 | mb:raymond-1999 | Raymond, Eric S. | The Cathedral and the Bazaar: Musings on Linux and Open Source by an Accidental Revolutionary (O'Reilly Linux) | book |
| 141 | mb:redhat-2017 | Red Hat | Increasing stability and predictability in Open Source license compliance: Providing a Fair Chance to Correct Mistakes | online |
| 142 | mb:reynolds-2008 | Reynolds, Garr | Presentation Zen: Simple Ideas on Presentation Design and Delivery | book |
| 143 | mb:riehle-2010 | Riehle, Dirk | The single-vendor commercial open course business model | article |
| 144 | mb:riehle-2010-foundations | Riehle, Dirk | The Economic Case for Open Source Foundations | article |
| 145 | mb:riehle-2014 | Riehle, Dirk and Riemer, Philipp and Kolassa, Carsten and Schmidt, Martin | Paid vs. Volunteer Work in Open Source | inproceedings |
| 146 | mb:rifkin-2014 | Rifkin, Jeremy | The zero marginal cost society: The internet of things, the collaborative commons, and the eclipse of capitalism | book |
| 147 | mb:roberts-et-al-2006 | Roberts, Jeffrey A. and Hann, Il-Horn and Slaughter, Sandra A. | Understanding the Motivations, Participation, and Performance of Open Source Software Developers: A Longitudinal Study of the Apache Projects | article |
| 148 | mb:robles-et-al-2012 | Robles, Gregorio and González-Barahona, Jesús M. | A Comprehensive Study of Software Forks: Dates, Reasons and Outcomes | inproceedings |
| 149 | mb:rogers-2016 | Mikael Rogers | Growing a contributor base in modern open source | online |
| 150 | mb:rosen-2016 | Rosen, Lawrence | Defining Open Standards | online |
| 151 | mb:sandler-2022 | Sandler, Karen | Foundations and Other Organisations | incollection |
| 152 | mb:scellato-et-al-2011 | Scellato, Giuseppe and Calderini, Mario and Caviggioli, Federico and Franzoni, Chiara and Ughetto, Elisa and Kica, Evisa and Rodriguez, Victor | Study on the quality of the patent system in Europe | reymisc |
| 153 | mb:schrape-2017 | Schrape, Jan-Felix | Open-source projects as incubators of innovation: From niche phenomenon to integral part of the industry | article |
| 154 | mb:schumpeter-1942 | Schumpeter, Joseph A | Capitalism, socialism and democracy | book |
| 155 | mb:sen-2007 | Ravi Sen | A Strategic Analysis of Competition Between Open Source and Proprietary Software | article |
| 156 | mb:shah-2006 | Shah, Sonali K. | Motivation, Governance, and the Viability of Hybrid Forms in Open Source Software Development | article |
| 157 | mb:shampanier-et-al-2007 | Shampanier, Kristina and Mazar, Nina and Ariely, Dan | Zero as a special price: The true value of free products | article |
| 158 | mb:shapiro-varian-1998 | Shapiro, Carl and Varian, Hal R. | Information Rules: A Strategic Guide to the Network Economy | book |
| 159 | mb:shin-2015 | Shin, Dong-Hee and Kim, Hongbum and Hwang, Junseok | Standardization revisited: A critical literature review on standards and innovation | article |
| 160 | mb:smith-2010 | Smith, Adam | The Wealth of Nations: An inquiry into the nature and causes of the Wealth of Nations | book |
| 161 | mb:smith-2022 | Smith, P McCoy | Copyright, Contract, and Licensing in Open Source | incollection |
| 162 | mb:spinellis-gousios-2009 | Spinellis, Diomidis and Gousios, Georgios | Beautiful Architecture: Leading Thinkers Reveal the Hidden Beauty in Software Design | book |
| 163 | mb:spivak-brenner-2001 | Spivak, Steven M. and Brenner, F. Cecil | Standardization Essentials: Principles and Practises | book |
| 164 | mb:spulber-2008 | Spulber, Daniel F. | Consumer coordination in the small and in the large: Implications for antitrust in markets with network effects | article |
| 165 | mb:stake-2010 | Stake, Robert E. | Qualitative Research: Studying How Things Work | book |
| 166 | mb:stallman-lessig-2010 | Stallman, Richard M. and Lessig, Lawrence | Free software, free society: selected essays of Richard M. Stallman | book |
| 167 | mb:stamm-2008 | Von Stamm, Bettina | Managing innovation, design and creativity | book |
| 168 | mb:suarez-utterback-1995 | Suárez, Fernando F. and Utterback, James M. | Dominant designs and the survival of firms | article |
| 169 | mb:tirole-2017 | Tirole, Jean | Economics for the common good | book |
| 170 | mb:torti-2015 | Torti, Valerio | Intellectual Property Rights and Competition in Standard Setting: Objectives and Tensions | book |
| 171 | mb:ullmann-margalit-1977 | Ullmann-Margalit, Edna | The emergence of norms | book |
| 172 | mb:updegrove-2015 | Updegrove, Andy | Licensing Standards that Include Code: Heads or Tails? | online |
| 173 | mb:varian-1995 | Varian, Hal R. | Pricing Information Goods | article |
| 174 | mb:varian-1997 | Varian, Hal R. | Buying, Sharing and Renting Information Goods | article |
| 175 | mb:varian-2005 | Varian, Hal R. | Copying and Copyright | article |
| 176 | mb:varian-2006 | Varian, Hal R. | Copyright term extension and orphan works | article |
| 177 | mb:vogt-2017 | Vogt, Martin | Deutsche Geschichte: von den Anfängen bis zur Wiedervereinigung | book |
| 178 | mb:weber-2004 | Weber, Steven | The Success of Open Source | book |
| 179 | mb:wef-2016 | World Economic Forum | Digital Transformation of Industries: Societal Implications | book |
| 180 | mb:whitehurst-2015 | Whitehurst, Jim | The open organization: Igniting passion and performance | book |
| 181 | mb:wiegmann-et-al-2017 | Paul Moritz Wiegmann and Henk J. de Vries and Knut Blind | Multi-mode standardisation: A critical review and a research agenda | article |
| 182 | mb:williams-2023 | Williams, Anthony | Standing Together on Shared Challenges: Report on the 2023 Open Source Congress | report |
| 183 | mb:wright-et-al-2023 | Nataliya Langburd Wright and Frank Nagle and Shane Greenstein | Open source software and global entrepreneurship | article |
| 184 | mb:yin-2003 | Yin, Robert K. | Case Study Research: Design and Methods | book |
| 185 | mb:young-1959 | Young, Michael Dunlop | The rise of the meritocracy, 1870-2033: The new elite of our social revolution | book |
| 186 | mb:yu-et-al-2014 | Yu, Yue and Yin, Gang and Wang, Huaimin and Wang, Tao | Exploring the Patterns of Social Behavior in GitHub | inproceedings |
