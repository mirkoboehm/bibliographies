#!/usr/bin/env python3

import bibtexparser
from bibtexparser.bwriter import BibTexWriter
from bibtexparser.customization import homogenize_latex_encoding, convert_to_unicode
import fileinput
import sys
import re
import argparse

Required_Python = (3, 6)
if sys.version_info < Required_Python:
    sys.exit("Python %s.%s or later is required.\n" % Required_Python)

Wanted_Python = (3, 7)
if sys.version_info < Wanted_Python:
    print("I wish this was Python %s.%s or later so that I could use sys.stdout.reconfigure..." % Wanted_Python, file=sys.stderr)

argparser = argparse.ArgumentParser(description='Merge and sort the bibliographies in the input files.')
argparser.add_argument('files', metavar='N', type=str, nargs='+',
                   help='input files')
argparser.add_argument('--format', default='bib', type=str,
                   help='Specify the output format (bib, md, default: bib)')
args = argparser.parse_args()

input = ""
for line in fileinput.input(files=args.files, openhook=fileinput.hook_encoded("utf-8")):
    input += line



parser = bibtexparser.bparser.BibTexParser(common_strings=True)
parser.ignore_nonstandard_types = False
parser.customization = homogenize_latex_encoding
parser.customization = convert_to_unicode
bibliography = parser.parse(input)

if len(bibliography.entries) != len(bibliography.entries_dict):
    entries = bibliography.entries[:]
    for key, value in bibliography.entries_dict.items():
        entries.remove(value)
    raise Exception("duplicate bibtex entry IDs in input data: {}".format(", ".join([entry['ID'] for entry in entries])))

def simplify(astring):
    return re.sub(r'\s+', ' ', astring)

# we always default to UTF-8 output:
# sys.stdout.reconfigure(encoding='utf-8')
sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)

if args.format == 'md':
    print("| {} | {} | {} | {} | {} |".format("#", "ID", "Author/Editor", "Title", "Type"))
    print("| --:|:-- |:-- |:-- |:-- |")
    sorted_entries = sorted(bibliography.entries, key=lambda e: e['ID'])
    person = ''
    for index, entry in enumerate(sorted_entries):
        if 'author' in entry:
            person = entry['author']
        elif 'editor' in entry:
            person = entry['editor']
        else:
            raise Exception("incomplete entry {}".format(entry['ID']))
        print(simplify("| {} | {} | {} | {} | {} |".format(index, entry['ID'], person, entry['title'], entry['ENTRYTYPE'])))
elif args.format == 'bib':
    writer = BibTexWriter()
    writer.order_entries_by = ('ID', 'ENTRYTYPE', 'author', 'year')
    sys.stdout.write(writer.write(bibliography))
else:
    raise Exception("unknown output format {}".format(args.format))

