#!/usr/bin/env python3
# This script takes a bibliography file with new entries, merges them into the bibliography
# and updates the overview table in the README.md file.

import argparse
import subprocess

argparser = argparse.ArgumentParser(description='Add new entries to the bibliography.')
argparser.add_argument('files', metavar='N', type=str, nargs='+', help='input files')
args = argparser.parse_args()

# Configuration:
bibliography = "Bibliography.bib"
readme = "README.md"
tmp_outfile = "Bibliography-new.bib"
tmp_table_md = "entry-table.md"
tmp_readme = "tmp-README.md"
marker = '## Entries'
print("Adding entries from {} to {} and updating {}...".format(", ".join(args.files), bibliography, readme))

try:
    subprocess.check_call("python3 ./Scripts/sort-bibliography.py {} {} > {}".format(bibliography, " ".join(args.files), tmp_outfile), shell=True)
    subprocess.check_call("mv {} {}".format(tmp_outfile, bibliography), shell=True)
    subprocess.check_call("python3 ./Scripts/sort-bibliography.py --format md {} > {}".format(bibliography, tmp_table_md), shell=True)
    with open(tmp_readme, 'w') as out:
        with open(tmp_table_md, 'r') as in_table:
            table = in_table.read()
            with open(readme, 'r') as in_readme:
                content = in_readme.read()
                entries_start = content.index(marker) + len(marker)
                out.write(content[:entries_start] + '\n\n' + table)
    subprocess.check_call("mv {} {}".format(tmp_readme, readme), shell=True)
    subprocess.check_call("rm {}".format(tmp_table_md), shell=True)
except subprocess.CalledProcessError as e:
    raise Exception("Adding bibliography entries failed: {}".format(str(e)))
